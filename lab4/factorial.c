#include <stdio.h>
#include <limits.h>  /* limits for integers */
#include <math.h>

long maxlong(void){

	return LONG_MAX;

}

double upper_bound(long n){

	if (n >= 6){
		return pow((n/2),n);
	
	}
	else{
		return 720;
	
	}

}

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    /*
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
    */
	getchar();
    return 0;
}


